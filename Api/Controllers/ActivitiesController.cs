using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Application.Activities;
using Domain;
using MediatR;
using Microsoft.AspNetCore.Mvc;

[Route ("api/[controller]")]
[ApiController]
public class ActivitiesController : ControllerBase {
    private readonly IMediator _mediator;

    public ActivitiesController (IMediator mediator) {
        this._mediator = mediator;
    }

    [HttpGet]
    public async Task<ActionResult<IEnumerable<Activity>>> List (CancellationToken ct) {
        return await this._mediator.Send (new List.Query (), ct);
    }

    [HttpGet ("{id}")]
    public async Task<ActionResult<Activity>> Get (Guid id) {
        return await this._mediator.Send (new Detail.Query (id));
    }

    [HttpPost]
    public async Task<ActionResult<Unit>> Create (Create.ActivityCreateCommand command) {
        return await this._mediator.Send (command);
    }

    [HttpPut ("{id}")]
    public async Task<ActionResult<Unit>> Edit (Guid id, Edit.EditCommand command) {
        command.Id = id;
        return await this._mediator.Send (command);
    }

    [HttpDelete ("{id}")]
    public async Task<ActionResult<Unit>> Delete (Guid id) {
        return await this._mediator.Send (new ActivitiesDeleteCommand { Id = id });
    }
}