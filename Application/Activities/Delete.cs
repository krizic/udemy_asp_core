using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Persistance;

namespace Application.Activities
{
    public class ActivitiesDeleteCommand : IRequest
    {
        public Guid Id { get; set; }
    }

    public class Handler : IRequestHandler<ActivitiesDeleteCommand>
    {
        private readonly DataContext _context;
        public Handler(DataContext context)
        {
            this._context = context;

        }

        public async Task<Unit> Handle(ActivitiesDeleteCommand request, CancellationToken cancellationToken)
        {

            var activity = await this._context.Activities.FindAsync(request.Id);

            if (activity == null) throw new Exception("No activity to delete");

            this._context.Remove(activity);

            var success = await this._context.SaveChangesAsync() > 0;

            if (success) return Unit.Value;

            throw new Exception("Problem saving changes");
        }
    }
}