const { codegen } = require("swagger-axios-codegen");
codegen({
    remoteUrl: "http://localhost:5000/swagger/v1/swagger.json",
    outputDir: "./src/api/",
    serviceNameSuffix: "",
    extendDefinitionFile: ""
}).catch(e => {
    console.log(e);
});
