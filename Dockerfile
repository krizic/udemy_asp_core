FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build
WORKDIR /app

# copy csproj and restore as distinct layers
COPY *.sln .
COPY Api/*.csproj ./Api/

# copy everything else and build app
COPY . .

RUN dotnet restore
WORKDIR /app/Api
RUN dotnet publish -c Release -o out


FROM mcr.microsoft.com/dotnet/core/aspnet:3.1 AS runtime
WORKDIR /app
COPY --from=build /app/Api/out ./
EXPOSE 80
ENTRYPOINT ["dotnet", "Api.dll"]